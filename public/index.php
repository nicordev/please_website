<?php

define('ROUTE_HOME', '/');
define('ROUTE_CURRENT_WEEK', '/what-is-the-current-week');
define('ROUTE_BASE64_DECODE', '/decode-base64');
define('ROUTE_TIMER', '/timer');
define('ROUTE_PWA', '/pwa');
define('ROOT', __DIR__.'/..');

$uri = $_SERVER['REQUEST_URI'];

if ($uri === ROUTE_CURRENT_WEEK) {
    echo 'Current week: ' . (new DateTime())->format("W");
} elseif (0 === strpos($uri, ROUTE_BASE64_DECODE)) {
    foreach ($_GET as $key => $value) {
        echo base64_decode($key)."\n";
    }
} elseif (0 === strpos($uri, ROUTE_TIMER)) {
    require ROOT.'/template/timer/timer.html.php';
} elseif (0 === strpos($uri, ROUTE_PWA)) {
    require ROOT.'/template/pwa/pwa.html';
} else {
    require ROOT.'/template/home/home.html.php';
}
